#include "wifi.h"
#include "credentials.h"

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <functional>

static bool _wifiConnected = false;

// connect to wifi – returns true if successful or false if not
bool wifiInit() {
	bool state = true;
	int i = 0;

	//	ledsColor(127, 127, 0); // show yellow when attempting to connect

	WiFi.begin(g_networkSSID, g_networkPassword);
	WiFi.persistent(false);
	WiFi.mode(WIFI_OFF);
	WiFi.mode(WIFI_STA);
	WiFi.begin(g_networkSSID, g_networkPassword);

	// Wait for connection
	Serial.print("Connecting ...");
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
		if (i > 10) {
			state = false;
			break;
		}
		i++;
	}

	if (state) {
		Serial.println("");
		Serial.print("Connected to ");
		Serial.println(g_networkSSID);
		Serial.print("IP address: ");
		Serial.println(WiFi.localIP());
		_wifiConnected = true;
	} else {
		Serial.println("");
		Serial.println("Connection failed.");
		_wifiConnected = false;
	}

	delay(250); // give the user a moment to see the color status
	return _wifiConnected;
}

bool wifiIsConnected() {
	return _wifiConnected;
}

char *wifiAddress() {
#define IPADDRSIZE 16
	static char buffer[IPADDRSIZE + 1];
	IPAddress ip = WiFi.localIP();
	snprintf(buffer, IPADDRSIZE, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
	return buffer;
}

#define OTA_PORT 8266 // its the default :-)

bool otaInit() {
	if (_wifiConnected) {
		ArduinoOTA.setPort(OTA_PORT);
		ArduinoOTA.setHostname(g_otaHostname);
		ArduinoOTA.setPassword(g_otaPassword);

		ArduinoOTA.onStart([]() {
			String type;
			if (ArduinoOTA.getCommand() == U_FLASH) {
				type = "sketch";
			} else { // U_SPIFFS
				type = "filesystem";
			}

			// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
			Serial.println("Start OTA update of " + type);
		});

		ArduinoOTA.onEnd([]() {
			Serial.println("\nEnd OTA update");
		});

		ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
			// blick the onboard LED (on then off)
			digitalWrite(2, LOW); delay(50); digitalWrite(2, HIGH); delay(50);

			Serial.printf("Progress of OTA update: %u%%\r", (progress / (total / 100)));
		});

		ArduinoOTA.onError([](ota_error_t error) {
			Serial.printf("OTA update rrror[%u]: ", error);
			if (error == OTA_AUTH_ERROR) {
				Serial.println("Auth Failed");
			} else if (error == OTA_BEGIN_ERROR) {
				Serial.println("Begin Failed");
			} else if (error == OTA_CONNECT_ERROR) {
				Serial.println("Connect Failed");
			} else if (error == OTA_RECEIVE_ERROR) {
				Serial.println("Receive Failed");
			} else if (error == OTA_END_ERROR) {
				Serial.println("End Failed");
			}
		});
		ArduinoOTA.begin();
		return true;
	}
	return false;
}

void otaLoop() {
	if (_wifiConnected) {
#if 0
		static bool _allow_ota = true;
 		if (_allow_ota) {
			// only allow OTA during the first 30 seconds; this prevents loop congestion which messes up Alexa access
			if (millis() > 30000)
				_allow_ota = false;
		}
#endif

		static unsigned long _test_cycle = 0;

		// we allow testing for OTA periodically
		if (millis() > _test_cycle) {
			ArduinoOTA.handle();
			_test_cycle += 2000; // 2 seconds from now
		}
	}
}
