/*
 *  This sketch demonstrates how to scan WiFi networks. 
 *  The API is almost the same as with the WiFi Shield library, 
 *  the most obvious difference being the different file you need to include:
 */

#include "wifi.h"

#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>


// the loop function runs over and over again forever
#define CODE_SHORT 100
#define CH_DIT CODE_SHORT
#define CH_DAH (CODE_SHORT * 3)
#define CH_BREAK (CODE_SHORT * 3)
#define WD_BREAK (CODE_SHORT * 7)
#define LED_ON LOW
#define LED_OFF HIGH

void dit() {
	digitalWrite(2, LED_ON);  // turn the LED on (HIGH is the voltage level)
	delay(CH_DIT);			  // wait for a second
	digitalWrite(2, LED_OFF); // turn the LED off by making the voltage LOW
	delay(CH_DIT);			  // wait for a second
}
void dah() {
	digitalWrite(2, LED_ON);  // turn the LED on (HIGH is the voltage level)
	delay(CH_DAH);			  // wait for a second
	digitalWrite(2, LED_OFF); // turn the LED off by making the voltage LOW
	delay(CH_DIT);			  // wait for a second
}

void setup() {
	Serial.begin(115200);
	Serial.setTimeout(2000);

	randomSeed(analogRead(0));

	// initialize digital pin 13 as an output.
	pinMode(2, OUTPUT);

	// Wait for serial to initialize.
	while (!Serial) {}

	Serial.println("\n\n");
	Serial.println("I'm awake.");

	if (!wifiInit()) {
		// we failed to get wifi so we broadcast S O S three times
		for (int i = 0; i < 3; i++) {
			dit(); dit(); dit(); delay(CH_BREAK);
			dah(); dah(); dah(); delay(CH_BREAK);
			dit(); dit(); dit(); delay(CH_BREAK);
			delay(WD_BREAK);
		}
	}
	else {
		// anything we may want on SUCCESS
	}

	otaInit();
}

int digit = 0;
void loop() {
	static char message[] = "hello world";
	// hello world
	// .... . .-.. .-.. ---    .-- --- .-. .-.. -..

	switch (message[digit]) {
		case 'h':
			dit(); dit(); dit(); dit();
			Serial.print("h");
			break;
		case 'e':
			dit();
			Serial.print("e");
			break;
		case 'l':
			dit(); dah(); dit(); dit();
			Serial.print("l");
			break;
		case 'o':
			dah(); dah(); dah();
			Serial.print("o");
			break;
		case ' ':
			Serial.print(" ");
			break;
		case 'w':
			dit(); dah(); dah();
			Serial.print("w");
			break;
		case 'r':
			dit(); dah(); dit();
			Serial.print("r");
			break;
		case 'd':
			dah(); dit(); dit();
			Serial.print("d");
			break;
		default:
			Serial.println("");
			break;
	}

	digit++;

	if (message[digit] == 0) {
		delay(2000);
		digit = 0;
		Serial.println("");
	} else if (message[digit] == ' ')
		delay(WD_BREAK);
	else
		delay(CH_BREAK);

	otaLoop();
}
