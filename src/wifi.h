#ifndef _WIFI_H
#define _WIFI_H

bool wifiIsConnected();
bool wifiInit();

char *wifiAddress();

bool otaInit();
void otaLoop();

#endif
