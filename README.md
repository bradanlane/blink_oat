# blink_oat

This is just a starter project with over-the-air update example. 

It will blink the onboard LED with **S-O-S** if it can not connect to the network.
If it successfully connects to the network, it will blink the onboard LED with **h-e-l-l-o  w-o-r-l-d**

To Use, rename (or copy) the `credentials.h-TEMPLATE` file to `credentials.h`
Then edit the file to set your network and OAT definitions.
